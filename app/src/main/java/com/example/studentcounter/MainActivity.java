package com.example.studentcounter;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {
    private Integer counter = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);//запрет на поворот экрана

    }

    public void onClickBtnAddStudents(View view) {
        counter++;
        TextView counterView = (TextView) findViewById(R.id.txt_counter);
        counterView.setText(counter.toString());
    }

    //метод сохранения данных перед поворотом
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("count 1", counter);
    }

    //метод воостановления после поворота
    @Override
    public void onRestoreInstanceState(Bundle savedInstanteState) {
        super.onRestoreInstanceState(savedInstanteState);
        counter = savedInstanteState.getInt("count 1");

    }

    @Override

    protected void onResume() {
        super.onResume();
        TextView counterView = (TextView) findViewById(R.id.txt_counter);
        counterView.setText(counter.toString());

    }

}


